-- -----------------------------------------------------
-- Base de datos FACULTAD
-- -----------------------------------------------------

#drop database facultad;
create database facultad;
use facultad;

create table ciudades(
	id_ciudad int not null auto_increment,
    nombre varchar(128) not null,

    primary key(id_ciudad)
) engine= InnoDB;

create table personas(
	id_persona int not null auto_increment,
    dni int unique not null,
    nombres varchar(128) not null,
    apellidos varchar(128) not null,
    ciudades_id_ciudad int not null,
    
    primary key(id_persona),
    foreign key(ciudades_id_ciudad) references ciudades(id_ciudad)
    on update cascade
    on delete cascade
) engine= InnoDB;

create table telefonos(
	id_telefono int not null auto_increment,
    numero_tel varchar(128) not null,
    tipo varchar(128) not null,
    personas_id_persona int not null,
    
    primary key(id_telefono),
    foreign key(personas_id_persona) references personas(id_persona)
)engine= InnoDB;

create table departamentos(
	id_departamento int not null auto_increment,
    nombre varchar(128) not null,
    nombre_corto varchar(8) not null,
    
    primary key (id_departamento)
) engine= InnoDB;

create table titulos(
		id_titulo int not null auto_increment,
        nombre varchar(128) unique not null,
        nombre_corto varchar(16),
        
        primary key (id_titulo)
) engine= InnoDB;

create table materias(
	id_materia int not null auto_increment,
    nombre varchar(128) unique not null,
    anio_en_plan int not null,
    
    primary key (id_materia)
) engine= InnoDB;

create table profesores(
	id_profesor int not null auto_increment,
    cuil varchar(13) unique not null,
    personas_id_persona int not null,
    materias_id_materia int,
    departamentos_id_departamento int not null,
    
    primary key (id_profesor),
    foreign key (personas_id_persona) references personas(id_persona),
    foreign key (materias_id_materia) references materias(id_materia),
    foreign key (departamentos_id_departamento) references departamentos(id_departamento)
    on update cascade
    on delete cascade
) engine= InnoDB;

create table estudiantes(
	id_estudiante int not null auto_increment,
    numero_alumno int unique not null,
    personas_id_persona int not null,
    ciudades_ciudad_residencia int not null,
    
    primary key (id_estudiante),
    foreign key (personas_id_persona) references personas(id_persona),
    foreign key (ciudades_id_ciudad) references ciudades(id_ciudad)
) engine= InnoDB;
# MODIFICACIÓN: NOMBRE DE COLUMNA
# Columna ciudades_id_ciudad renombrada por ciudades_ciudad_residencia
/*
alter table estudiantes
	change ciudades_id_ciudad ciudades_ciudad_residencia int not null;
*/
create table Profesores_has_Titulos(
	id_profesor_titulo int not null auto_increment,
    profesores_id_profesor int not null,
    titulos_id_titulo int not null,
    
    primary key(id_profesor_titulo),
    constraint profesores_id_profesor foreign key (profesores_id_profesor) references profesores (id_profesor),
    constraint titulos_id_titulo foreign key (titulos_id_titulo) references titulos (id_titulo)
    on update cascade
    on delete cascade
) engine= InnoDB;

create table Estudiantes_has_Materias(
	id_estudiante_materia int not null auto_increment,
    estudiantes_id_estudiante int not null,
    materias_id_materias int not null,
    anio_en_curso int not null,
    nota int,
    
    primary key(id_estudiante_materia),
    constraint estudiantes_id_estudiante foreign key (estudiantes_id_estudiante) references estudiantes (id_estudiante),
    constraint materias_id_materias foreign key (materias_id_materias) references materias (id_materia)
    on update cascade
    on delete cascade
) engine= InnoDB;

