-- ----------------------------------------------
-- OBTENCIÓN DE DATOS
-- ----------------------------------------------

use facultad;

# a - Listar cada una de las tablas con la totalidad de datos almacenados en ellas.
select *from ciudades;
select *from personas;
select *from departamentos;
select *from profesores;
select *from estudiantes;
select *from materias;
select *from telefonos;
select *from titulos;
select *from estudiantes_has_materias;
select *from profesores_has_titulos;

# b - Listar todos los profesores de:

# Un departamento puntual.
select pr.id_profesor, pe.dni, pe.nombres, pe.apellidos, d.id_departamento, d.nombre_corto 
	from departamentos as d
	inner join profesores as pr
		on pr.departamentos_id_departamento = d.id_departamento
	inner join personas as pe
		on pr.personas_id_persona = pe.id_persona
    where d.id_departamento = 2;

# Una materia puntual.
select pr.id_profesor, pe.dni, pe.nombres, pe.apellidos, m.id_materia, m.nombre 
	from materias as m
	inner join profesores as pr
		on pr.materias_id_materia = m.id_materia
	inner join personas as pe
		on pr.personas_id_persona = pe.id_persona
    where m.id_materia = 3;

# Que hayan nacido en una ciudad puntual.
select prof.id_profesor, per.nombres, per.apellidos, c.nombre
	from ciudades as c
	inner join personas as per
		on per.ciudades_id_ciudad = c.id_ciudad
	inner join profesores as prof
		on prof.personas_id_persona = per.id_persona
	group by prof.id_profesor order by c.nombre;
    
# c - Listar el nombre de departamento y la cantidad de docentes que posee cada uno.
select d.nombre_corto, count(*) as 'Cantidad docentes' from profesores as p 
	inner join departamentos as d
		on p.departamentos_id_departamento = d.id_departamento
	group by d.nombre_corto;


# d - Listar el nombre de la ciudad y la cantidad de personas que nacieron en ella.
select c.nombre, count(*) as 'Cantidad nacidos' from personas as p
	inner join ciudades as c
		on p.ciudades_id_ciudad = c.id_ciudad
	group by c.nombre order by c.nombre;

# e - Listar todos los alumnos.

# Que cursan actualmente una materia puntual.
select e.numero_alumno, p.dni, p.nombres, p.apellidos, m.id_materia, m.nombre as 'Materia'
	from estudiantes_has_materias as ehm
	inner join materias as m
		on ehm.materias_id_materias = m.id_materia
	inner join estudiantes as e
		on ehm.estudiantes_id_estudiante = e.id_estudiante
	inner join personas as p
		on e.personas_id_persona = p.id_persona
	where m.id_materia = 1 and ehm.nota is null;
    
# Que aprobaron una materia puntual.
select e.numero_alumno, p.dni, p.nombres, p.apellidos, m.id_materia, m.nombre as 'Materia', ehm.nota
	from estudiantes_has_materias as ehm
    inner join materias as m
		on ehm.materias_id_materias = m.id_materia
	inner join estudiantes as e
		on ehm.estudiantes_id_estudiante = e.id_estudiante
	inner join personas as p
		on e.personas_id_persona = p.id_persona
	where ehm.nota >= 7 order by m.id_materia;

# Que residen en una ciudad puntual.
select e.numero_alumno, p.dni, p.nombres, e.ciudades_ciudad_residencia as 'ID ciudad', c.nombre
	from estudiantes as e
	inner join personas as p
		on e.personas_id_persona = p.id_persona
	inner join ciudades as c
		on e.ciudades_ciudad_residencia = c.id_ciudad
	order by c.id_ciudad;
	
/*
# OBTENGO LA CANTIDAD DE ESTUDIANTES RESIDENTES POR CADA CIUDAD
select c.nombre as 'Ciudad', count(e.id_estudiante)
	from ciudades as c
    inner join estudiantes as e
		on c.id_ciudad = e.ciudades_ciudad_residencia
	group by c.nombre;
*/

# f. Listar los datos del alumno con menor nota de materia.
select *from estudiantes_has_materias where nota is not null;
select *from estudiantes;
select *from personas;

select *from estudiantes as e
	inner join personas as p
		on p.id_persona = e.personas_id_persona
	where e.id_estudiante = (select estudiantes_id_estudiante
							from estudiantes_has_materias
								where nota is not null
								group by id_estudiante_materia order by nota asc
								limit 1);
                                    
# PRUEBAS DE COMO USAR EL GROUP BY JUNTO AL ORDER BY Y LA FUNCION MIN/MAX
/*
select id_estudiante, numero_alumno
	from estudiantes
		group by id_estudiante order by numero_alumno desc;

select estudiantes_id_estudiante
	from estudiantes_has_materias
		where nota is not null
		group by id_estudiante_materia order by min(nota) asc;
*/

# g. Listar los datos del alumno con mayor nota de materia.
select *from estudiantes as e 
	inner join personas as p
		on p.id_persona = e.personas_id_persona
	where e.id_estudiante = (select estudiantes_id_estudiante
							from estudiantes_has_materias
								where nota is not null
								group by materias_id_materias order by nota desc
								limit 1);

# h. Listar los datos de la materia y el promedio de las notas de sus alumnos.
select *from estudiantes_has_materias where materias_id_materias = 1 and nota is not null;
select *from materias;

select m.id_materia, m.nombre, avg(ehm.nota) as 'Nota promedio' from materias as m
	inner join estudiantes_has_materias as ehm
		on ehm.materias_id_materias = m.id_materia
	group by m.id_materia;
    
#i. Listar los datos de todas las personas (profesores y estudiantes) en una misma consulta.
select *from personas;