-- ----------------------------
-- Realizar los triggers para:
-- ----------------------------
use facultad;

/*1. Al momento de insertar una nueva persona, que se actualicé automáticamente el
valor de la cantidad de ciudadanos nacidos incrementándose en 1 el valor de la
ciudad correspondiente a la persona que se insertó.*/

DELIMITER //
create trigger u_increm_cant_nacidos
after insert on personas
for each row
begin
	update ciudades
	set cant_nacidos = (cant_nacidos + 1)
	where id_ciudad = new.ciudades_id_ciudad;
end //
DELIMITER //
#drop trigger if exists u_increm_cant_nacidos;
//

/*2. Al momento de eliminar una persona, que se actualicé automáticamente el valor
de la cantidad de ciudadanos nacidos decrementándose en 1 el valor de la
ciudad correspondiente a la persona que se eliminó.*/

DELIMITER //
create trigger u_decrem_cant_nacidos
after delete 
on personas for each row
begin
	update ciudades
	set cant_nacidos = (cant_nacidos - 1)
	where id_ciudad = old.ciudades_id_ciudad;
end //
DELIMITER //
#drop trigger if exists u_increm_cant_nacidos;
//

/*3. Al momento de insertar un nuevo estudiante, que se actualicé automáticamente
el valor de la cantidad de ciudadanos residentes incrementándose en 1 el valor
de la ciudad correspondiente al estudiante que se insertó.*/

DELIMITER //
create trigger u_increm_cant_residentes
after insert
on estudiantes for each row
begin
	update ciudades
	set cant_residentes = (cant_residentes + 1)
	where id_ciudad = new.ciudades_ciudad_residencia;
end //
DELIMITER //
#drop trigger if exists u_increm_cant_residentes;
//

/*4. Al momento de eliminar un nuevo estudiante, que se actualicé automáticamente
el valor de la cantidad de ciudadanos residentes decrementándose en 1 el valor
de la ciudad correspondiente al estudiante que se eliminó.*/

DELIMITER //
create trigger u_decrem_cant_residentes
after delete
on estudiantes for each row
begin
	update ciudades
	set cant_residentes = (cant_residentes - 1)
	where id_ciudad = old.ciudades_ciudad_residencia;
end //
DELIMITER //
#drop trigger if exists u_decrem_cant_residentes;
//

/*5. Al momento de insertar un nuevo profesor, que se actualicé automáticamente el
valor de la cantidad de profesores en ese departamento incrementándose en 1 el
valor del departamento correspondiente al profesor que se insertó.*/

DELIMITER //
create trigger u_increm_empleado
after insert
on profesores for each row
begin
	update departamentos
	set cant_empleados = (cant_empleados + 1)
	where id_departamento = new.departamentos_id_departamento;
end //
DELIMITER //
#drop trigger if exists u_increm_empleado;
//

/*6. Al momento de eliminar un nuevo profesor, que se actualicé automáticamente el
valor de la cantidad de profesores en ese departamento decrementándose en 1
el valor del departamento correspondiente al profesor que se insertó.*/

DELIMITER //
create trigger u_decrem_empleado
after delete
on profesores for each row
begin
	update departamentos
	set cant_empleados = (cant_empleados - 1)
	where id_departamento = old.departamentos_id_departamento;
end //
DELIMITER //
#drop trigger if exists u_decrem_empleado;
//

/*7. Al momento de insertar un nuevo profesor que dicta una materia, que se
actualicé automáticamente el valor de la cantidad de profesores que dictan esa
materia incrementándose en 1 el valor de la materia correspondiente al profesor
que se insertó.*/

-- CREAR TABLA INTERMEDIA 

DELIMITER //
create trigger u_increm_prof_materia
after insert
on profesores for each row
begin
	update materias
	set cant_profesores = (cant_profesores + 1)
	where id_materia = new.materias_id_materia;
end //
DELIMITER //
#drop trigger if exists u_increm_prof_materia;
//

/*8. Al momento de eliminar un profesor que dicta una materia, que se actualicé
automáticamente el valor de la cantidad de profesores que dictan esa materia
decrementándose en 1 el valor de la materia correspondiente al profesor que se
eliminó.*/

DELIMITER //
create trigger u_decrem_prof_materia
after delete
on profesores for each row
begin
	update materias
	set cant_profesores = (cant_profesores - 1)
	where id_materia = old.materias_id_materia;
end //
DELIMITER //
#drop trigger if exists u_decrem_prof_materia;
//

/*9. Al momento de anotar un estudiante en una materia, que se actualicé
automáticamente el valor de la cantidad de inscriptos a esa materia
incrementándose en 1 el valor de la cantidad de estudiantes a la materia
correspondiente al estudiante que se insertó.*/

DELIMITER //
create trigger increm_inscripto_ai
after insert 
on estudiantes_has_materias
for each row
begin
	update materias
	set cant_inscriptos = (cant_inscriptos + 1)
	where id_materia = new.materias_id_materias;
end //
DELIMITER //
#drop trigger if exists increm_inscripto_ai
//

/*10. Al momento de eliminar un estudiante en una materia, que se actualicé
automáticamente el valor de la cantidad de inscriptos a esa materia
decrementándose en 1 el valor de inscriptos a la materia correspondiente al
estudiante que se eliminó.*/

DELIMITER //
create trigger decrem_inscripto_ad
after delete 
on estudiantes_has_materias
for each row
begin
	update materias
	set cant_inscriptos = (cant_inscriptos - 1)
	where id_materia = old.materias_id_materias;
end //
DELIMITER //
#drop trigger if exists decrem_inscripto_ad;
//

/*11. Al momento de anotar un estudiante en una materia, si este posee una nota de
materia ya, que se actualicé automáticamente el valor de la cantidad de
aprobados o desaprobados según corresponda a esa materia. (nota < 7 desaprobado)*/

DELIMITER //
create trigger increm_aprob_desaprob_ai
after insert
on estudiantes_has_materias
for each row
begin
		if new.nota < 7 then 
			update materias 
			set cant_desaprobados = (cant_desaprobados + 1)
            where id_materia = new.materias_id_materias;
        else
			if new.nota > 7 then
				update materias 
				set cant_aprobados = (cant_aprobados + 1)
				where id_materia = new.materias_id_materias;
			end if;
		end if;
end //
DELIMITER //
#drop trigger if exists increm_aprob_desaprob_ai;
//

/*12. Al momento de actualizar la nota de un estudiante que ya existía, que se
actualicé automáticamente el valor de la cantidad de aprobados o desaprobados
según corresponda a esa materia. (nota < 7 desaprobado)*/

DELIMITER //
create trigger increm_aprob_desaprob_update_au
after update
on estudiantes_has_materias
for each row
begin
		if (old.nota >= 7 and new.nota < 7) then
			update materias
			set cant_aprobados = (cant_aprobados - 1), cant_desaprobados = (cant_desaprobados + 1)
            where id_materia = new.materias_id_materias;
        elseif (old.nota < 7 and new.nota >= 7) then
			update materias
			set cant_desaprobados = (cant_desaprobados - 1), cant_aprobados = (cant_aprobados + 1)
            where id_materia = new.materias_id_materias;
		elseif (old.nota is null and new.nota >= 7) then
			update materias
            set cant_aprobados = cant_aprobados = (cant_aprobados + 1)
            where id_materia = new.materias_id_materias;
		elseif (old.nota is null and new.nota < 7) then
			update materias
            set cant_desaprobados = cant_desaprobados = (cant_desaprobados + 1)
            where id_materia = new.materias_id_materias;
		end if;
end //
DELIMITER //
//
drop trigger if exists increm_aprob_desaprob_update_au;
//

-- EXTRA -->
-- Actualiza los valores de cant_aprobados o cant_desaprobados en el caso de un DELETE del registro
-- Decremento en 1 la condición de la materia en caso de que se elimine el registro de la materia cursada por un estudiante*/

DELIMITER //
create trigger increm_aprob_desaprob_update_ad
after delete
on estudiantes_has_materias
for each row
begin
		if (old.nota) then
			if (old.nota > 7) then
				update materias 
                set cant_aprobados = (cant_aprobados - 1)
                where id_materia = old.materias_id_materias;
			else
				update materias 
                set cant_desaprobados = (cant_desaprobados - 1)
                where id_materia = old.materias_id_materias;
			end if;
		end if;
end //
DELIMITER //