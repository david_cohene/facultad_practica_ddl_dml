-- -----------------------
-- Realizar los SPs para:
-- -----------------------
use facultad;

/*1. Listar cada una de las tablas con la totalidad de datos almacenados en ellas.*/

# CIUDADES
DELIMITER //
create procedure listar_ciudades()
	begin
		select * from ciudades;
	end //
DELIMITER //
//
call listar_ciudades();
//

# DEPARTAMENTOS
DELIMITER //
create procedure listar_departamentos()
	begin
		select * from departamentos;
	end //
DELIMITER //
//
call listar_departamentos();
//

# ESTUDIANTES
DELIMITER //
create procedure listar_estudiantes()
	begin
		select * from estudiantes;
	end //
DELIMITER //
//
call listar_estudiantes();
//

# ESTUDIANTES_HAS_MATERIAS
DELIMITER //
create procedure listar_estudiantes_has_materias()
	begin
		select * from estudiantes_has_materias;
	end //
DELIMITER //
//
call listar_estudiantes_has_materias();
//

# MATERIAS
DELIMITER //
create procedure listar_materias()
	begin
		select * from materias;
	end //
DELIMITER //
//
call listar_materias();
//

# PERSONAS
DELIMITER //
create procedure listar_personas()
	begin
		select * from personas;
	end //
DELIMITER //
//
call listar_personas();
//

# PROFESORES
DELIMITER //
create procedure listar_profesores()
	begin
		select * from profesores;
	end //
DELIMITER //
//
call listar_profesores();
//

# PROFESORES_HAS_TITULOS
DELIMITER //
create procedure listar_profesores_has_titulos()
	begin
		select * from profesores_has_titulos;
	end //
DELIMITER //
//
call listar_profesores_has_titulos();
//

# TELEFONOS
DELIMITER //
create procedure listar_telefonos()
	begin
		select * from telefonos;
	end //
DELIMITER //
//
call listar_telefonos();
//

# TITULOS
DELIMITER //
create procedure listar_titulos()
begin
	select * from titulos;
end //
DELIMITER //
//
call listar_titulos();
//

/*2. Listar todos los profesores de:*/

/*a. Un departamento puntual.*/
DELIMITER //
create procedure sp_profesores_depto_2()
begin
	select pr.id_profesor, pe.dni, pe.nombres, pe.apellidos, d.id_departamento, d.nombre_corto 
	from departamentos as d
	inner join profesores as pr
		on pr.departamentos_id_departamento = d.id_departamento
	inner join personas as pe
		on pr.personas_id_persona = pe.id_persona
    where d.id_departamento = 2;
end //
DELIMITER //
//
call sp_profesores_depto_2();
//

/*b. Una materia puntual.*/
DELIMITER //
create procedure sp_profesores_mat_3()
begin
	select pr.id_profesor, pe.dni, pe.nombres, pe.apellidos, m.id_materia, m.nombre 
	from materias as m
	inner join profesores as pr
		on pr.materias_id_materia = m.id_materia
	inner join personas as pe
		on pr.personas_id_persona = pe.id_persona
    where m.id_materia = 3;
end //
DELIMITER //
//
call sp_profesores_mat_3();
//

/*c. Que hayan nacido en una ciudad puntual.*/
DELIMITER //
create procedure sp_profesores_ciud_nac()
begin
	select prof.id_profesor, per.nombres, per.apellidos, c.nombre
	from ciudades as c
	inner join personas as per
		on per.ciudades_id_ciudad = c.id_ciudad
	inner join profesores as prof
		on prof.personas_id_persona = per.id_persona
	group by prof.id_profesor order by c.nombre;
end //
DELIMITER //
//
call sp_profesores_ciud_nac();
//

/*3. Listar el nombre de departamento y la cantidad de docentes que posee cada uno.*/
DELIMITER //
create procedure sp_cant_docent_depto()
begin
	select d.nombre_corto, count(*) as 'Cantidad docentes' from profesores as p 
	inner join departamentos as d
		on p.departamentos_id_departamento = d.id_departamento
	group by d.nombre_corto;
end //
DELIMITER //
//
call sp_cant_docent_depto();
//

/*4. Listar el nombre de la ciudad y la cantidad de personas que nacieron en ella.*/
DELIMITER //
create procedure sp_cant_nacim_ciudad()
begin
	select c.nombre, count(*) as 'Cantidad nacidos' from personas as p
	inner join ciudades as c
		on p.ciudades_id_ciudad = c.id_ciudad
	group by c.nombre order by c.nombre;
end //
DELIMITER //
//
call sp_cant_nacim_ciudad();
//

/*5. Listar todos los alumnos.*/

/*a. Que cursan actualmente una materia puntual.*/
DELIMITER //
create procedure sp_curso_materia_1()
begin
	select e.numero_alumno, p.dni, p.nombres, p.apellidos, m.id_materia, m.nombre as 'Materia'
	from estudiantes_has_materias as ehm
	inner join materias as m
		on ehm.materias_id_materias = m.id_materia
	inner join estudiantes as e
		on ehm.estudiantes_id_estudiante = e.id_estudiante
	inner join personas as p
		on e.personas_id_persona = p.id_persona
	where m.id_materia = 1 and ehm.nota is null;
end //
DELIMITER //
//
call sp_curso_materia_1()
//

/*b. Que aprobaron una materia puntual.*/
DELIMITER //
create procedure sp_aprobados_materias()
begin
	select e.numero_alumno, p.dni, p.nombres, p.apellidos, m.id_materia, m.nombre as 'Materia', ehm.nota
	from estudiantes_has_materias as ehm
    inner join materias as m
		on ehm.materias_id_materias = m.id_materia
	inner join estudiantes as e
		on ehm.estudiantes_id_estudiante = e.id_estudiante
	inner join personas as p
		on e.personas_id_persona = p.id_persona
	where ehm.nota >= 7 order by m.id_materia;
end //
DELIMITER //
//
call sp_aprobados_materias();
//

/*c. Que residen en una ciudad puntual.*/
DELIMITER //
create procedure sp_residen_ciudades_2(in idCiudad int )
begin
	select e.numero_alumno, p.dni, p.nombres, e.ciudades_ciudad_residencia as 'ID ciudad', c.nombre
	from estudiantes as e
	inner join personas as p
		on e.personas_id_persona = p.id_persona
	inner join ciudades as c
		on e.ciudades_ciudad_residencia = c.id_ciudad
	where e.ciudades_ciudad_residencia = idCiudad
    order by c.id_ciudad;
end //
DELIMITER //
//
call sp_residen_ciudades_2(4);
//

/*6. Listar los datos del alumno con menor nota de materia.*/
DELIMITER //
create procedure sp_estud_menor_nota()
begin
	select *from estudiantes as e
	inner join personas as p
		on p.id_persona = e.personas_id_persona
	where e.id_estudiante = (select estudiantes_id_estudiante
							from estudiantes_has_materias
								where nota is not null
								group by id_estudiante_materia
                                order by nota asc
								limit 1);
end //
DELIMITER //
//
call sp_estud_menor_nota();
//

/*7. Listar los datos del alumno con mayor nota de materia.*/
DELIMITER //
create procedure sp_estud_mayor_nota()
begin
	select *from estudiantes as e 
	inner join personas as p
		on p.id_persona = e.personas_id_persona
	where e.id_estudiante = (select estudiantes_id_estudiante
								from estudiantes_has_materias
								where nota is not null
								group by materias_id_materias
                                order by nota desc
								limit 1);
end //
DELIMITER //
//
call sp_estud_mayor_nota();
//

/*8. Listar los datos de la materia y el promedio de las notas de sus alumnos.*/
DELIMITER //
create procedure sp_nota_prom_materias()
begin
	select m.id_materia, m.nombre as 'Materia', avg(ehm.nota) as 'Nota promedio' from materias as m
	inner join estudiantes_has_materias as ehm
		on ehm.materias_id_materias = m.id_materia
	group by m.id_materia;
end //
DELIMITER //
//
call sp_nota_prom_materias();
//

/*9. Listar los datos de todas las personas (profesores y estudiantes) en una misma consulta.*/
//
call sp_listar_personas();
//