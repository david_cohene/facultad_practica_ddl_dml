use facultad;

insert into 
	ciudades (nombre)
values
	('Berazategui'),
    ('Claypole'),
    ('Ezpeleta'),
	('Florencio Varela'),
    ('La Plata'),
    ('Quilmes');

#select * from ciudades;

insert into
	personas (dni, nombres, apellidos, ciudades_id_ciudad)
values
	(11111111, 'Ezequiel', 'Ruskin', 1),
    (22222222, 'Leonardo', 'Gutierrez', 1),
    (33333333, 'Fernandinho', 'Castolo', 4),
    (44444444, 'Sergio', 'Minanda', 3),
    (55555555, 'Andrés', 'Ettori', 2),
    (66666666, 'Daniel', 'Ordaz', 2),
    (77777777, 'Analia', 'Stremer', 1),
	(88888888, 'Nicolas', 'Hamsun', 2),
    (99999999, 'Pablo', 'Dodo', 2),
    (10101010, 'Agostina', 'Valeny', 1),
    (11011011, 'Elisabet', 'Stein', 4),
	(12012012, 'Thomas', 'Huylens', 6),
	(13013013, 'Lucas', 'Jaric', 5),
    (14014014, 'José', 'Espimas', 2),
    (15015015, 'Lionel', 'Ivarov', 5),
    (16016016, 'Rubén', 'Macco', 5),
    (17017017, 'Alma', 'Ximelez', 4),
    (18018018, 'Camila', 'Iouga', 6);
    
#select * from personas;

insert into
	materias (nombre, anio_en_plan)
values
    ('Programación I', 1),
    ('Biología para Ciencias de la Salud', 1),
    ('Principios de economía I', 2);
    
#select * from materias;

insert into
	departamentos (nombre, nombre_corto)
values
	('Departamento Ciencias Sociales', 'DTO_SOC'),
	('Departamento Ingeniería e Informática', 'DTO_ING'),
    ('Departamento Salud', 'DTO_SAL');
 
#select * from departamentos;

insert into
	titulos (nombre, nombre_corto)
values
	('Ingeniería en Informática','Ing. Informático'),
    ('Licenciatura en Medicina y Cirujía','Lic. Med. y Cir.'),
    ('Doctorado en Ciencas económicas','Dr. en Economía'),
    ('Ingeniería en Telecomunicaciones','Ing. Telecomunic'),
    ('Ingeniería en Agronomía','Ing. Agrónomo'),
    ('Máster en Instrumentación y Tecnología Química','Máster en Quím'),
    ('Licenciatura en Administración de Empresas','Lic. en Administ'),
    ('Ingeniería en Sistemas', 'Ing. Sistemas');
    
#select *from titulos;

insert into
	profesores (cuil, personas_id_persona, materias_id_materia, departamentos_id_departamento)
values
	('20-11111111-2',1,2,3),	#1
	('27-77777777-7',7,1,2),	#2
    ('20-10101010-2',10,1,2),	#3
    ('23-33333333-8',3,1,2),	#4
    ('24-11011011-3',11,3,1),	#5
    ('20-88888888-8',8,2,3),	#6
    ('20-16016016-8',16,2,3),	#7
    ('20-18018018-8',18,3,1),	#8
    ('20-99999999-8',9,3,1);	#9

#select * from profesores;
/*
select p.id_profesor, d.id_departamento, d.nombre_corto from profesores as p
	inner join departamentos as d
	on p.departamentos_id_departamento= d.id_departamento;
*/

# CONSULTA PARA VISUALIZAR LA TABLA titulos Y EL DEPTO DEL PROFESOR PARA LUEGO ASIGNARLE UN TITULO ACORDE A SU DEPTO
/*
select t.id_titulo, t.nombre_corto, p.id_profesor, d.nombre_corto
	from titulos as t cross join profesores as p, departamentos as d 
	where p.departamentos_id_departamento = d.id_departamento;
*/

insert into
	profesores_has_titulos (profesores_id_profesor, titulos_id_titulo)
values
	(5, 3),	#1
    (8, 7),	#2
    (9, 3),	#3
    (7, 6),	#4
    (6, 2),	#5
    (1, 2),	#6
	(4, 4),	#7
	(3, 5),	#8
    (2, 8);	#9
    
select *from profesores_has_titulos;

insert into
	estudiantes (numero_alumno, personas_id_persona, ciudades_ciudad_residencia)
values
	(4545, 2, 4),	#1
    (4852, 4, 6),	#2
    (5301, 5, 9),	#3
    (5910, 6, 4),	#4
    (5589, 12, 4),	#5
    (4960, 13, 4),	#6
    (4810, 14, 7),	#7
    (5515, 15, 6),	#8
    (4502, 17, 1);	#9
    
#select * from estudiantes;

# 9 estudiantes con la materia 1
insert into
	estudiantes_has_materias (estudiantes_id_estudiante, materias_id_materias, anio_en_curso)
values
	(1, 1, 2),
    (2, 1, 2),
    (3, 1, 2),
    (4, 1, 3),	
    (5, 1, 2),
    (6, 1, 3),
    (7, 1, 1),
    (8, 1, 3),
    (9, 1, 2);
    
# 6 estudiantes con la materia 2
insert into
	estudiantes_has_materias (estudiantes_id_estudiante, materias_id_materias, anio_en_curso)
values
	(1, 2, 2),
	(2, 2, 2),
    (3, 2, 2),
    (4, 2, 3),
    (6, 2, 3),
    (8, 2, 3);	
   
# 3 estudiantes con la materia 3
insert into
	estudiantes_has_materias (estudiantes_id_estudiante, materias_id_materias, anio_en_curso)
values
	(1, 3, 2),
    (2, 3, 2),
    (3, 3, 2);


# VISUALIZAR LAS materias CURSADAS ACTUALMENTE POR CADA ESTUDIANTE
/*
select e.id_estudiante, ehm.materias_id_materias, ehm.nota from estudiantes e
	inner join estudiantes_has_materias ehm 
    on e.id_estudiante = ehm.estudiantes_id_estudiante
    where ehm.nota is null
    order by id_estudiante;
*/

# VISUALIZAR A LOS estudiantes Y LAS materias QUE FINALIZÓ PARA ASIGNARLE LA NOTA
/*
select * from estudiantes_has_materias
	where estudiantes_id_estudiante = 9;
*/

# AGREGO EL VINCULO DE LOS estudiantes CON materias FALTANTES CON SU NOTA
insert into
	estudiantes_has_materias (estudiantes_id_estudiante, materias_id_materias, anio_en_curso, nota)
values
	(4, 3, 3, 8),
	(5, 2, 2, 5),
    (5, 3, 2, 8),
    (6, 3, 3, 7),
    (7, 2, 1, 6),
    (7, 3, 1, 8),
    (8, 3, 3, 7),
    (9, 2, 2, 7),
    (9, 3, 2, 6);

#select *from estudiantes_has_materias;

insert into
	telefonos (numero_tel, tipo, personas_id_persona)
values
	(11423433,'móvil',1),
    (4586422,'fijo',1),
    (1123850020,'móvil',7),
    (4688720,'fijo',7),
    (1135010188,'móvil',10),
    (49534200,'fijo',10),
    (1122101150, 'móvil', 11),
    (48501001, 'fijo', 3),
    (22164208941, 'móvil', 13),
    (1100124887, 'móvil', 2),
    (1128151509, 'móvil', 5),
    (1140112301, 'móvil', 4),
    (45600255, 'fijo', 4),
    (1121581104,'móvil', 18);

#select *from telefonos;

